# Site web VdL

Un petit dépôt pour tester et montrer comment on fabrique du logiciel de nos jours. 
Alors j'avoue que je maîtrise pas grand chose, mais je serai contente d'apprendre !  
L'avantage des sources du site web des VdL c'est qu'elles sont extrêmement simples (3 ou 4 fichiers) et ne nécessitent pas de serveur ou d'interpréteur en dehors d'un navigateur web. 

### DONE

Ce 26/01 :
- j'ai créé le dépôt, réussi à synchro local sur mon poste à partir des sources "anciennes", 
- j'ai aussi constaté la restriction de commit directement sur main (obligation de faire des branches?).
- j'ai invité Thierry P. en tant que "maintainer" et Jerome T. (par mail) en tant que "developpeur". _(je reste très disposée à changer ces rôles si ça vous convient pas hein, c'était pour tester la palette)_


### ToDO

Absolument rien d'obligatoire mais peut-être : 
- Thierry peux-tu reverser ici le HTML de la version "à jour" car je n'ai plus d'accès FTP ? 
- Se voir pour comparer les différents accès de nos comptes ? 

- ira-t-on jusqu'à l'automatisation des mises en ligne avec [les GitLab Pages?](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html)
on verra bien ! 